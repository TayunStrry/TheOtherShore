# THE_OTHER_SHORE - Dream
[中文](./README.md)
![输入图片说明](pack_icon.png)

#### Introduction
We earnestly anticipate a mod for the Bedrock Edition that can gradually align with the Java Edition.

Hence, we present the **Bedrock Blueprint Project**.

Hereinafter referred to as the **Bedrock Blueprint**.

In the current planning phase, the **Bedrock Blueprint** is divided into the following three domains:

1. **Opal Port**, built upon the **Server API**, serves as a framework. It aims to implement various creative functionalities through limited and straightforward parameters.

2. **Arcane Industry**, fully based on **Custom Blocks** and grounded in **Block Tags**, is an endeavor to assess whether industrial automation can be entirely achieved through data-driven mechanisms.

3. **Otherworldly Beings**, built upon **Custom Entities**, aims to create more engaging combat skills and character functionalities.

Please note that this codebase contains numerous Chinese variables. If you have any concerns, kindly voice them elsewhere. We only accept feedback related to functional and logical bugs.

![输入图片说明](pack_icon1.gif)